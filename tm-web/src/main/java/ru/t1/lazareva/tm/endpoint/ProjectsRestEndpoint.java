package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.ProjectDto;
import ru.t1.lazareva.tm.model.CustomUser;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @GetMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public List<ProjectDto> get(@AuthenticationPrincipal final CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(user.getUserId(), projects);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(user.getUserId(), projects);
    }

    @DeleteMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(@AuthenticationPrincipal final CustomUser user) {
        projectService.removeAll(user.getUserId());
    }

}