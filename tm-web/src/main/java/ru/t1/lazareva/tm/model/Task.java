package ru.t1.lazareva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import ru.t1.lazareva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "task")
public final class Task {

    @Column(name = "name")
    protected String name = "";
    @Column(name = "description")
    protected String description = "";
    @Enumerated(value = EnumType.STRING)
    protected Status status = Status.NOT_STARTED;
    @Column(name = "date_start")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    protected Date dateStart;
    @Column(name = "date_finish")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    protected Date dateFinish;
    @Id
    private String id = UUID.randomUUID().toString();
    @ManyToOne
    private Project project;

    @ManyToOne
    private User user;

    public Task() {
    }

    public Task(final String name) {
        this.name = name;
    }

}