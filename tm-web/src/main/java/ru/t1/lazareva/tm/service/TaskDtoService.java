package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDto;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.NameEmptyException;
import ru.t1.lazareva.tm.exception.UserIdEmptyException;
import ru.t1.lazareva.tm.repository.ITaskDtoRepository;
import ru.t1.lazareva.tm.repository.IUserDtoRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Transactional
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Autowired
    private IUserDtoRepository userRepository;

    @SneakyThrows
    @Override
    @Transactional
    public void save(@Nullable final String userId, @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task.getId() == null || task.getId().isEmpty()) throw new IdEmptyException();
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        task.setUserId(userId);
        repository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final String userId, @Nullable final Collection<TaskDto> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks.isEmpty()) throw new IdEmptyException();
        for (@NotNull TaskDto taskDto : tasks) {
            if (taskDto.getName() == null || taskDto.getName().isEmpty()) throw new NameEmptyException();
            taskDto.setUserId(userId);
        }
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDto findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

}