package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.dto.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService {

    void save(final String userId, final TaskDto task);

    void saveAll(final String userId, final Collection<TaskDto> tasks);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<TaskDto> findAll(final String userId);

    TaskDto findOneById(final String userId, final String id);

}