package ru.t1.lazareva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.ProjectDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.CustomUser;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/project/create")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        projectService.save(user.getUserId(), new ProjectDto("New Project" + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeOneById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") ProjectDto project,
            BindingResult result
    ) {
        projectService.save(user.getUserId(), project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView edit(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") String id) {
        final ProjectDto project = projectService.findOneById(user.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}