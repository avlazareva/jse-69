package ru.t1.lazareva.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.dto.TaskDto;

import java.util.List;

public interface TasksRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks";

    static TasksRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TasksRestEndpointClient.class, BASE_URL);
    }

    @GetMapping()
    List<TaskDto> get();

    @PostMapping
    void post(@RequestBody List<TaskDto> tasks);

    @PutMapping
    void put(@RequestBody List<TaskDto> tasks);

    @DeleteMapping()
    void delete();

}