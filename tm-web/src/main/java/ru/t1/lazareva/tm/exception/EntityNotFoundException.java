package ru.t1.lazareva.tm.exception;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
