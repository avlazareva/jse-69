package ru.t1.lazareva.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.dto.TaskDto;

public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/task";

    static TaskRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/{id}")
    TaskDto get(@PathVariable("id") String id);

    @PostMapping
    void post(@RequestBody TaskDto task);

    @PutMapping
    void put(@RequestBody TaskDto task);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") String id);

}
