package ru.t1.lazareva.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CredentialsDto {

    protected String username;

    protected String password;

}