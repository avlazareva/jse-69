package ru.t1.lazareva.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

}