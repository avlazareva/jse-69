package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.ProjectDto;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    void deleteAllByUserId(@NotNull String userId);

    void deleteByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDto findByIdAndUserId(@NotNull String id, @NotNull String userId);

}
