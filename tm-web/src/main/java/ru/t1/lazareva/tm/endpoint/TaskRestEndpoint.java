package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDto;
import ru.t1.lazareva.tm.model.CustomUser;

import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService
public class TaskRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskDto get(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody TaskDto task) {
        taskService.save(user.getUserId(), task);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody TaskDto task) {
        taskService.save(user.getUserId(), task);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
    }

}