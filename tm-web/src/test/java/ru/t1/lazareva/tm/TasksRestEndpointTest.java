package ru.t1.lazareva.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.lazareva.tm.client.TasksRestEndpointClient;
import ru.t1.lazareva.tm.dto.TaskDto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TasksRestEndpointTest {

    @NotNull
    final TasksRestEndpointClient client = TasksRestEndpointClient.client();

    @NotNull
    final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    final List<TaskDto> tasks = Arrays.asList(task1, task2, task3);

    @NotNull
    final TaskDto task4 = new TaskDto("Test Task 4");

    @NotNull
    final TaskDto task5 = new TaskDto("test Task 5");

    @NotNull
    final List<TaskDto> tasks2 = Arrays.asList(task4, task5);

    @Before
    public void init() {
        client.post(tasks);
    }

    @After
    public void clear() {
        client.delete();
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get());
        for (final TaskDto task : tasks) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPost() {
        Assert.assertEquals(3, client.get().size());
        client.post(tasks2);
        Assert.assertNotNull(client.get());
        Assert.assertEquals(5, client.get().size());
        for (final TaskDto task : tasks2) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPut() {
        for (final TaskDto project : client.get()) {
            Assert.assertNull(project.getDescription());
        }
        tasks.get(0).setDescription("Description 1");
        tasks.get(1).setDescription("Description 2");
        tasks.get(2).setDescription("Description 3");
        client.put(tasks);
        for (final TaskDto task : client.get()) {
            Assert.assertNotNull(task.getDescription());
        }
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get());
        client.delete();
        Assert.assertEquals(Collections.emptyList(), client.get());
    }

}
