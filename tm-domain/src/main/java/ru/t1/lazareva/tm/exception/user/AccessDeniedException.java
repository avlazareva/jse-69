package ru.t1.lazareva.tm.exception.user;

import ru.t1.lazareva.tm.exception.field.AbstractFieldException;

public final class AccessDeniedException extends AbstractFieldException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}