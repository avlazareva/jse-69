package ru.t1.lazareva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull
    @Value("#{environment['mongo.port']}")
    public Integer mongoPort;
    @NotNull
    @Value("#{environment['mongo.host']}")
    public String mongoHost;
    @NotNull
    @Value("#{environment['mongo.db']}")
    public String mongoDBName;
    @NotNull
    @Autowired
    private MongoDatabase mongoDatabase;

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

}