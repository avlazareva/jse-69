package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.model.IUserOwnedService;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;
import ru.t1.lazareva.tm.repository.model.AbstractUserOwnedRepository;

import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends AbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected R repository;

    @NotNull
    @Autowired
    protected UserService userService;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUser(userService.findOneById(userId));
        repository.save(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteById(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAllByUserId(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IdEmptyException();
        return repository.findOneByUserIdAndIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSizeByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        repository.removeByUserId(userId, model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @Nullable M result = findOneById(userId, id);
        repository.removeByUserId(userId, result);
    }

    @Override
    @SneakyThrows
    @Transactional
    public M update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        resultModel = repository.updateByUserId(userId, model);
        return resultModel;
    }

}